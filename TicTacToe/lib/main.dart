import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'gameML.dart';

const bs = BorderSide(color: Colors.white24, width: 5);
const borderDef = [
  Border(bottom: bs, right: bs),
  Border(
    bottom: bs,
    right: bs,
    left: bs,
  ),
  Border(bottom: bs, left: bs),
  Border(
    bottom: bs,
    right: bs,
    top: bs,
  ),
  Border(bottom: bs, right: bs, top: bs, left: bs),
  Border(bottom: bs, top: bs, left: bs),
  Border(right: bs, top: bs),
  Border(right: bs, top: bs, left: bs),
  Border(top: bs, left: bs),
];

const iconsDef = {
  FieldType.none: null,
  FieldType.circle: Icons.radio_button_unchecked,
  FieldType.cross: Icons.close
};

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
        title: 'TicTacToe',
        theme: ThemeData.from(colorScheme: ColorScheme.dark()),
        home: Scaffold(
          appBar: AppBar(title: Text("TicTacToe")),
          body: Game(),
        ));
  }
}

class RButton extends StatelessWidget {
  final String caption;
  final VoidCallback onClick;

  RButton({@required this.caption, this.onClick});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onClick,
      child: Text(caption),
    );
  }
}

class RField extends StatelessWidget {
  final _GameState gameState;
  final GameML gameML;
  final int pos;

  RField(this.gameState, this.pos) : gameML = gameState.gameML;

  Icon getIcon() {
    if ((gameML.finishedGame()) && (gameML.isCrossLine(pos))) {
      return Icon(iconsDef[gameML.getField(pos)], color: Colors.red, size: 110);
    } else {
      return Icon(iconsDef[gameML.getField(pos)], size: 90);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 130,
      height: 130,
      decoration: BoxDecoration(
        border: borderDef[pos],
      ),
      child: IconButton(
        onPressed: gameML.finishedGame()
            ? null
            : () {
                gameState.nextMove(pos);
              },
        icon: getIcon(),
        // ),
      ),
    );
  }
}

class Game extends StatefulWidget {
  @override
  _GameState createState() => _GameState();
}

class _GameState extends State<Game> {
  final GameML gameML = new GameML();

  void nextMove(int pos) {
    gameML.nextMove(pos);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RButton(
              caption: "2 players",
              onClick: () {
                gameML.startGame(GameType.twoPlayers);
                setState(() {});
              },
            ),
            RButton(
                caption: "Start Computer",
                onClick: () {
                  gameML.startGame(GameType.computerDump, startComputer: true);
                  setState(() {});
                }),
            RButton(
                caption: "Start Player",
                onClick: () {
                  gameML.startGame(GameType.computerExpert);
                  setState(() {});
                }),
          ],
        ),
        Spacer(
          flex: 1,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RField(this, 0),
            RField(this, 1),
            RField(this, 2),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RField(this, 3),
            RField(this, 4),
            RField(this, 5),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RField(this, 6),
            RField(this, 7),
            RField(this, 8),
          ],
        ),
        Spacer(
          flex: 1,
        ),
      ],
    );
  }
}
