import 'dart:math';

enum GameType { twoPlayers, computerDump, computerExpert }
enum FieldType { none, cross, circle }

const crossFinish = [
  [
    [1, 2],
    [3, 6],
    [4, 8]
  ],
  [
    [0, 2],
    [4, 7]
  ],
  [
    [0, 1],
    [4, 6],
    [5, 8]
  ],
  [
    [4, 5],
    [0, 6]
  ],
  [
    [3, 5],
    [1, 7],
    [0, 8],
    [2, 6]
  ],
  [
    [3, 4],
    [2, 8]
  ],
  [
    [0, 3],
    [7, 8],
    [2, 4]
  ],
  [
    [6, 8],
    [1, 4]
  ],
  [
    [6, 7],
    [2, 5],
    [0, 4]
  ]
];

class GameML {
  bool _finishedGame = true;
  GameType gameType = GameType.twoPlayers;
  List<FieldType> _board = new List(9);
  int moveCount;

  List<int> getListOfPos() {
    return _board
        .asMap()
        .entries
        .map((e) => (e.value == FieldType.none) ? e.key : -1)
        .where((e) => (e >= 0))
        .toList();
  }

  bool finishedGame() {
    if (_finishedGame) return true;
    if (moveCount > 8) return true;
    return false;
  }

  bool _isEmdGameInPos(int pos, FieldType ft) {
    assert(ft != FieldType.none);

    final a = crossFinish[pos];

    for (var element in a) {
      final okCount = element.where((pos) => _board[pos] == ft).length;

      if (okCount == 2) return true;
    }
    return false;
  }

  bool isCrossLine(int pos) {
    return (_board[pos] == FieldType.none)
        ? false
        : _isEmdGameInPos(pos, _board[pos]);
  }

  int randomMove() {
    var listOfPos = getListOfPos();
    final listOfEndGame = listOfPos
        .where((pos) => _isEmdGameInPos(pos, _getFieldTypeByMove(moveCount)))
        .toList();

    if (listOfEndGame.isNotEmpty) {
      _finishedGame = true;
      return listOfEndGame[0];
    }

    final listOfEnd = listOfPos.where(
        (pos) => _isEmdGameInPos(pos, _getFieldTypeByMove(moveCount + 1)));
    if (listOfEnd.isNotEmpty) listOfPos = listOfEnd.toList();

    var random = new Random();
    if (listOfPos.contains(4)) if (random.nextInt(10) >= 5) return 4;

    int randomPos = random.nextInt(listOfPos.length);
    return listOfPos[randomPos];
  }

  int bestMove(int moveCount) {
    var listOfPos = getListOfPos();
    final listOfEndGame = listOfPos
        .where((pos) => _isEmdGameInPos(pos, _getFieldTypeByMove(moveCount)));

    if (listOfEndGame.isNotEmpty) {
      _finishedGame = true;
      return listOfEndGame.first;
    }

    final listOfEnd = listOfPos.where(
        (pos) => _isEmdGameInPos(pos, _getFieldTypeByMove(moveCount + 1)));

    if (listOfEnd.isNotEmpty) listOfPos = listOfEnd.toList();

    var random = new Random();
    if (listOfPos.contains(4)) if (random.nextInt(10) >= 5) return 4;

    int randomPos = random.nextInt(listOfPos.length);
    return listOfPos[randomPos];
  }

  void startGame(GameType type, {bool startComputer = false}) {
    _board.fillRange(0, _board.length, FieldType.none);

    _finishedGame = false;
    gameType = type;
    moveCount = 0;

    if (startComputer && (type != GameType.twoPlayers)) nextMove(-1);
  }

  FieldType _getFieldTypeByMove(int moveCounter) {
    return ((moveCounter % 2) == 0) ? FieldType.cross : FieldType.circle;
  }

  void setMove(int movePos) {
    if (_board[movePos] != FieldType.none) throw Exception();

    _board[movePos] = _getFieldTypeByMove(moveCount);
    moveCount++;
  }

  FieldType getField(int pos) {
    return _board[pos];
  }

  void nextMove(int movePos) {
    try {
      if (finishedGame()) return;

      if (movePos >= 0) {
        if (_isEmdGameInPos(movePos, _getFieldTypeByMove(moveCount)))
          _finishedGame = true;

        setMove(movePos);
        if (finishedGame()) return;
      }
      switch (gameType) {
        case GameType.twoPlayers:
          break;

        case GameType.computerDump:
          var move = randomMove();
          setMove(move);
          break;

        case GameType.computerExpert:
          var move = randomMove();
          setMove(move);
          break;
      }
    } catch (Exception) {}
  }
}
